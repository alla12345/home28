import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getNumber } from '../../actionCreators';
import './Button.scss';

class Button extends Component {

  handleClick = () => {
    const { getNumber } = this.props;
    getNumber();
  }

  render() {
    const { value, isLoading } = this.props;
    return (
      <button className = 'cr' onClick={this.handleClick}>{ isLoading ? '...' : value }</button>
    );
  }
}

const mapStateToProps = ({ numbers }) => ({
  value: numbers.value,
  isLoading: numbers.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  getNumber: () => dispatch(getNumber()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Button);
