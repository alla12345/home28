export function getNumber(number) {
  return {
    type: 'FETCH_NUMBER',
    payload: {
      number,
    },
  };
}
