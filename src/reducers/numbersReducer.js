const initState = {
  value: 5,
  isLoading: false,
};

function reducer(state = initState, action) {
  switch (action.type) {
    case 'SUCCES_FETCH_NUMBER': {
      return {
        ...state,
        value: action.payload.number,
        isLoading: false,
      };
    }
    case 'START_FETCH_NUMBER': {
      return {
        ...state,
        isLoading: true,
      };
    }
    // case 'FAILED_ADD': {
    //   return {
    //     ...state,
    //     isLoading: false,
    //   };
    // }
    default:
      return state;
  }
}

export default reducer;
