import { combineReducers } from 'redux';
import numbers from './numbersReducer';

export default combineReducers({
  numbers,
});
