import { all } from 'redux-saga/effects';
import getSaga from './getSaga';

export default function* rootSaga() {
  yield all([getSaga()]);
}
