// import { put, takeEvery, call, throttle } from 'redux-saga/effects';
import { put, takeEvery, call } from 'redux-saga/effects';

// export function fetchPostsApi(reddit) {
//   return fetch(`https://www.reddit.com/r/${reddit}.json`)
//     .then((response) => response.json())
//     .then((json) => json.data.children.map((child) => child.data));
// }
const delay = (ms) => new Promise ((res) => setTimeout(res, ms));

export function* fetchNumber(action) {
  yield put({ type: 'START_FETCH_NUMBER' });    
  // yield delay(2000);
  yield call(delay, 2000);
  const number = Math.floor(Math.random() * 10);
  yield put ({type:'SUCCES_FETCH_NUMBER', payload: {number}});

  
  // try {
  //   const ans = yield call(fetchPostsApi, action.payload.reddit);
  //   yield put({
  //     type: 'SUCCES_ADD',
  //     payload: {
  //       number: number,
  //     },
  //   });
  // } catch (e) {
  //   yield put({ type: 'FAILED_ADD' });
  // }
}

export default function* getSaga() {
  yield takeEvery('FETCH_NUMBER', fetchNumber);
  // yield throttle(1000, 'FETCH_ADD', incrementAsync);
}
